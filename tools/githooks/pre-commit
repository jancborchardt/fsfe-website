#!/usr/bin/env sh

GIT_ROOT=$(git rev-parse --show-toplevel)

# CHECK XML SYNTAX
TESTS=0
for xmlfile in $(
  git diff --staged --name-only | grep -E "(\.xhtml$|\.xml$|\.xsl$)"
); do
  if [ -e "${xmlfile}" ]; then
    xmllint --noout --nonet "${xmlfile}"
    TESTS=$((TESTS + $?))
  fi
done

if [ $TESTS -gt 0 ]; then

cat <<EOF >&2

  === SYNTAX ERROR ===
  One or more files failed the XML syntax check!

  The error log above will help you to identify the error. Read it from
  the top. The numbers behind the file name point to the line number in
  the file where the error has been detected.

  Check this line and its surroundings for XML/HTML tags that have not 
  been closed correctly, errors with special characters like "&", or
  other syntactical mistakes.

  The commit has been aborted. You have to fix the problem first.
EOF

  exit $TESTS
fi

# CHECK FOR NEWLY INTRODUCED TAGS
NEWTAGS=0
FILES=""
# test all newly staged (added) XML files in /news and /events
for xmlfile in $(
  git diff --staged --name-only | grep -E "^(news/|events/).*(\.xhtml$|\.xml$|\.xsl$)"
); do
  hit=0
  tags=""
  # go through all tags in this file
  # make only a new line a field separator to support tags with spaces inside (which is not recommended)
  OLDIFS=$IFS
  IFS=$'\n'
  for tag in $(grep -Ei "<tag.*?>.+?</tag>" "${xmlfile}" | sed -E "s|.*<tag.*?>(.+?)</tag>.*|\1|g"); do
    # check if this tag does exist in any other news/event item
    if ! git grep -irlE "<tag.*?>${tag}</tag>" news/ events/ | grep -vq "${xmlfile}"; then
      hit=1
      tags="${tag}, ${tags}"
      NEWTAGS=$((NEWTAGS + 1))
    fi
  done
  IFS=$OLDIFS   # reset IFS, usually " \t\n"
  # if any new tag has been found, enlist them
  if [ $hit != 0 ]; then
    tags="${tags%, }"
    FILES="${FILES}|${xmlfile} (new tag(s): ${tags})"
  fi
done

if [ $NEWTAGS -gt 0 ]; then
  cat <<EOF >&2
  === NEW / DUPLICATED TAG(S) ===
  Your commit introduced $NEWTAGS tag(s) which did not exist before in
  our news or event items!
  $(echo "${FILES}" | sed -E -e "s/\|/\n  - /g")

  Please make sure that you use already used tags, and only introduce a
  new tag e.g. if it's about a new campaign that will be more often
  mentioned in news or events. If you feel unsure, please ask
  <web@lists.fsfe.org>.

  Here you will find the currently used tags:
  https://fsfe.org/tags/tags.html

  Your commit has been executed anyway. Please make another commit to
  replace a new tag with an already existing one unless you are really
  sure. Thank you.

EOF

fi


# CHECK TRANSLATIONS WHICH WILL BE OUTDATED
ALLFILES=$(git diff --staged --name-only | grep -E "(\.xhtml$|\.xml$)")
ENFILES=$(echo "${ALLFILES}" | grep -E "\.en\.")
OUTDATED=0
TEXT=

for file in $ENFILES; do
  WARN=
  # Get file extension
  EXT="${file##*.}"
  # remove "en.$EXT"
  BASE=$(echo "${file}" | sed -E "s/\.[a-z][a-z]\.$EXT//")
  LANGS_UP=$("${GIT_ROOT}"/tools/check-translation-status.sh -f "${file}" -o up)

  # check whether previously up-to-date translations will be in this commit
  for trans in $LANGS_UP; do
    trans="${BASE}.${trans}.${EXT}"
    if ! echo "${ALLFILES}" | grep -Eq "^${trans}$"; then
      OUTDATED=$((OUTDATED + 1))
      WARN="${WARN}, ${trans}"
    fi
  done
  WARN=$(echo "${WARN}" | sed -E "s/^, //")
  # only add to list of outdated files if at least 1 warning has been found
  if [ -n "${WARN}" ]; then
    TEXT="${TEXT}|${file} => ${WARN}"
  fi
done

if [ $OUTDATED -gt 0 ]; then
  cat <<EOF >&2
  === OUTDATE WARNING ===
  Your commit caused ${OUTDATED} previously up-to-date translations 
  to become outdated!

  If you made non-content changes to the English base files, 
  consider making a fake-commit to these translations:

  - $(echo "${TEXT}" | sed -E -e "s/^\|//" -e "s/\|/\n  - /g")

  Read more: https://wiki.fsfe.org/TechDocs/Mainpage/Translations/Outdated

EOF

fi
